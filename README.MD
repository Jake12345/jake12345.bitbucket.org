Submission README
=========
---


Personal Details (Students Fill In Below)
===
---
- **Student Name**:Jake Holland
- **Student Number**: 10502837


- **Course**: Computing & Games Development
- **Module**: AINT103
- **Assignment Title**: Zombie Shooter Launch Website

1. **Live Website Link**: http://jake12345.bitbucket.org/about.html
2. **Blog Link**: http://jakeholland95.tumblr.com/
3. **Credits**
 * Web Template - Interactive Systems Studio 
 * http://www.clipartpal.com/clipart_pd/holiday/halloween/trickortreat_10004.html ( free domain picture )
---


Assignment Details
===

---
###Deadline
**Group 2 - Tuesday 16th Dec 2014 @ 18:00**

**Group 1 - Friday 19th Nov 2014 @ 18:00**

---
###Module Documentation
[AINT103 Module Overview](https://dle.plymouth.ac.uk/pluginfile.php/239059/mod_resource/content/1/AINT103OverviewAssignmentSpecification.pdf)

---
###Submission Requirements
- You have added **iss-plymouth** as a user to your repository (see [instructions here](http://homepage.iss.io/bitbucket-add-user.html))
- Provide the following files:
    2. A deployable website with AINT102 Unity web build embeded.
    4. A link to your development blog
    5. A complete README.md
--- 
###Submission Folder Layout

```
/
├── README.md
├── index.html
├── ... 
```

---
###Assignment Briefing
Develop a website to launch your first Unity project; hand coded website in HTML enhanced with CSS and Javascript
with embedded Unity asset (developed from AINT102). Minimum of 3 pages (Launch page, Bio Page & Portfolio page).
You will be checked against the following list:

* Handcoded Validated HTML
* Handcoded Validated CSS
* Javascript include
    * Comments
    * Error free
    * Passes Linting
* Evidence of Cross Browser testing (i.e. Browsershots screenshots)
* Evidence of accessibility testing (i.e. WAVE Report)
* Evidence of completed exercises
    * Embedded Unity project
    * Javascript Experiments
* Effective use of version control with complete Repo
* Live website (via Bitbucket or other host)

#####See Module Overview for full details.